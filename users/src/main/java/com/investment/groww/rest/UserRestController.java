package com.investment.groww.rest;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.investment.groww.dao.UserDAO;
import com.investment.groww.entity.User;
import com.investment.groww.service.UserService;

@RestController
@RequestMapping("/groww")
@EnableCaching
public class UserRestController {
	
	private UserService userService;
	
	@Autowired
	public UserRestController(UserService theUserService) {
		userService = theUserService;
	}
	
	Logger logger = LoggerFactory.getLogger(UserRestController.class);
	
	//method for getting list of all users
	@GetMapping("/users")
	public List<User> getAll(){
		
		logger.info("retrieving all users data");
		
		return userService.getAll();
	}
	
	//method for writing new user data
	@PostMapping("/users")
	public User addUser(@Valid @RequestBody User theUser) {
		
		logger.info("writing new data");
		
		theUser.setId(0);
		userService.saveUser(theUser);
		return theUser;
		
	}
	
	//method for updating the existing data
	@PutMapping("/users")
	public User updateUser(@RequestBody User theUser) {
		
		logger.info("updating data");
		
		userService.saveUser(theUser);
		return theUser;
	}
	
	//method for updating the existing data
	@DeleteMapping("/users/{userId}")
	@CacheEvict(key="userId", value = "User")
	public String deleteUser(@PathVariable int userId) {
		
		logger.info("deleting data with id: " + userId);

		userService.deleteUser(userId);
		return "Deleted user id - ";
	}
	
	//method for getting user by Id
	@GetMapping("/users/{userId}")
	@Cacheable(value = "User",key = "#userId")
	public List<User> getbyId(@PathVariable int userId){
	
	logger.info("retrieving particular data with id: " + userId);
		return userService.getbyId(userId);
		
	}
	
	
}
