package com.investment.groww;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class GrowwApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrowwApplication.class, args);
	}

}
