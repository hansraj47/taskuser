package com.investment.groww.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.investment.groww.entity.User;

@Repository
public class UserDAOImpl implements UserDAO {

	private EntityManager entityManager;
	
	@Autowired
	public UserDAOImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	
	@Override
	public List<User> getAll() {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		Query<User> theQuery =
				theSession.createQuery("from User",User.class);
		
		List<User> userdetails = theQuery.getResultList();
		
		
		return userdetails;
	}

	@Override
	public void saveUser(User theUser) {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		theSession.saveOrUpdate(theUser);
		
	}

	@Override
	public void deleteUser(int theId) {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		Query theQuery = 
				theSession.createQuery(
						"delete from User where id=:userId");
		theQuery.setParameter("userId", theId);
		
		theQuery.executeUpdate();
	}

	@Override
	public List<User> getbyId(int theId) {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		Query<User> theQuery =
				theSession.createQuery("from User where id=:userId",User.class);
		theQuery.setParameter("userId", theId);
		
		List<User> userdetails = theQuery.getResultList();
		
		
		return userdetails;
	}

}
