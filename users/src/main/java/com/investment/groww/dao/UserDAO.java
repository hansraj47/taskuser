package com.investment.groww.dao;

import java.util.List;

import com.investment.groww.entity.User;

public interface UserDAO {
	
	public List<User> getAll();
	
	public List<User> getbyId(int theId);
	
	public void saveUser(User theUser);
	
	public void deleteUser(int theId);
	
}
