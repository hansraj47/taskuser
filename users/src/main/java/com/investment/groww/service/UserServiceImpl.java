package com.investment.groww.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.investment.groww.dao.UserDAO;
import com.investment.groww.entity.User;

@Service
public class UserServiceImpl implements UserService {

	private UserDAO userDAO;
	
	@Autowired
	public UserServiceImpl(UserDAO theUserDAO) {
		userDAO = theUserDAO;
	}
	
	@Override
	@Transactional
	public List<User> getAll() {
		
		return userDAO.getAll();
	}

	@Override
	@Transactional
	public void saveUser(User theUser) {
	
		userDAO.saveUser(theUser);
		
	}

	@Override
	@Transactional
	public void deleteUser(int theId) {
		
		userDAO.deleteUser(theId);
	}


	@Override
	@Transactional
	public List<User> getbyId(int theId) {
		// TODO Auto-generated method stub
		return userDAO.getbyId(theId);
	}

}
