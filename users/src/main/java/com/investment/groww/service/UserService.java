package com.investment.groww.service;

import java.util.List;

import com.investment.groww.entity.User;

public interface UserService {
	
	
	// service methods for getting list of users, usersbyId, updating users and deleting users
	
	public List<User> getAll();
	
	public List<User> getbyId(int theId);
	
	public void saveUser(User theUser);
	
	public void deleteUser(int theId);


}
