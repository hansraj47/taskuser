package com.investment.groww.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.NonNull;

@Entity
@Table(name="user")
public class User implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@NotNull
	@NotBlank
	@Size(min = 2, message = "First Name should have atleast 2 characters")
	@Column(name="first_name")
	private String firstName;
	
	@NotBlank
	@Column(name="last_name")
	private String lastName;
	
	@NotBlank
	@Column(name="date_of_birth")
	private String dateofBirth;
	
	@NotBlank
	@Column(name="gender")
	private String gender;
	
	@NotBlank
	@NotNull
	@Size(min=12,max=12)
	@Column(name="pan_no")
	private String panNo;
	
	@NotBlank
	@Size(min=3,max=25)
	@Column(name="address")
	private String address;
	
	@NotBlank
	@Column(name="account_no")
	private int accountNumber;
	
	@NotBlank
	@Column(name="ifsc_code")
	private String IFSCCode;
	
	public User() {
		
	}

	public User(String firstName, String lastName, String dateofBirth, String gender, String panNo, String address,
			int accountNumber, String iFSCCode) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateofBirth = dateofBirth;
		this.gender = gender;
		this.panNo = panNo;
		this.address = address;
		this.accountNumber = accountNumber;
		IFSCCode = iFSCCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDateofBirth() {
		return dateofBirth;
	}

	public void setDateofBirth(String dateofBirth) {
		this.dateofBirth = dateofBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIFSCCode() {
		return IFSCCode;
	}

	public void setIFSCCode(String iFSCCode) {
		IFSCCode = iFSCCode;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dateofBirth=" + dateofBirth
				+ ", gender=" + gender + ", panNo=" + panNo + ", address=" + address + ", accountNumber="
				+ accountNumber + ", IFSCCode=" + IFSCCode + "]";
	}
	

}
