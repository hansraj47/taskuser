package com.investment.growwrestTest;



import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.investment.groww.dao.UserDAO;
import com.investment.groww.entity.User;
import com.investment.groww.rest.UserRestController;

@WebMvcTest(UserRestController.class)
public class UserRestControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private JsonMapper objectMapper;
	
	@MockBean
	private UserDAO userDAO;
	
	@Test
	public void testgetAll() throws Exception {
		List<User> listUsers = new ArrayList<>();
		listUsers.add(new User("Hansraj","Rouniyar","2015-06-09","Female","1239080","Brt",34098,"IFG342"));
		listUsers.add(new User("ansraj","ouniyar","2014-06-09","Female","1939080","Brt",340778,"IFOI342"));
		Mockito.when(userDAO.getAll()).thenReturn(listUsers);
		String url = "/groww/users";
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(url)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		
		String actualJsonResponse = mvcResult.getResponse().getContentAsString();
		System.out.println(actualJsonResponse);
		
		String expectedJsonResponse = objectMapper.writeValueAsString(listUsers);
		
		assert(actualJsonResponse).equals(expectedJsonResponse);
		
	}
	
	
	
}
